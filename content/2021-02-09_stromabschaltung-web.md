---toml
title = "Stromabschaltung WEB"
date = "2021-02-09T07:30:15.962Z"
severity = "major-outage"
affectedsystems = ["etherpad", "mumble", "scribble"]
resolved = true
modified = "2021-02-09T09:44:00.000Z"
---
Am Dienstag, dem 09.02.2021, ab 8 Uhr wird der gesamte Komplex Weberplatz kurzfristig abgeschaltet. Hintergrund ist die Beseitigung einer Ratte in der NSHV.

Falls die Abschaltdauer die Haltezeit der USV überteigt, kommt es zu Ausfällen des Campusnetzes.

https://betriebsstatus.zih.tu-dresden.de/?action=newsdet&news_id=945

::: update Resolved | 2021-02-09T09:44:00.000Z
Die Systeme sind wieder erreichbar.
:::

<!--- language code: de -->
