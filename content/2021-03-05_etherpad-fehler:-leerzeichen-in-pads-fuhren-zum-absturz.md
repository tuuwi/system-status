---toml
title = "Etherpad Fehler: Leerzeichen in Pads führen zum Absturz"
date = "2021-03-03T09:15:02.194Z"
severity = "degraded-performance"
affectedsystems = ["etherpad"]
resolved = true
modified = "2021-05-11T10:13:32.665Z"
---
Wenn ein Leerzeichen bei der Suche eingegeben wird, stürzt Etherpad ab und wird neu gestartet. Das führt dazu, dass einige noch nicht gespeicherte Änderungen verloren gehen können. Solang der Fehler besteht, bitte Leerzeichen durch Unterstriche (_) in der Suche ersetzen.

<!--- language code: de -->
