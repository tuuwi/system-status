---toml
title = "AG Film Test"
date = "2021-05-11T10:11:59.863Z"
severity = "partial-outage"
affectedsystems = ["scribble"]
resolved = true
modified = "2021-10-28T10:22:51.695Z"
---
Für einen Test Livestream für die AG Film wird scribble abgeschaltet.

<!--- language code: de -->
